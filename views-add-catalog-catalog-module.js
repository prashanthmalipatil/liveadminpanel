(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-add-catalog-catalog-module"],{

/***/ "./src/app/views/add catalog/add-catalog.component.css":
/*!*************************************************************!*\
  !*** ./src/app/views/add catalog/add-catalog.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".hero_home.version_1{background:url(/assets/customer.jpg) center bottom no-repeat #3f4079}.hero_home{height:180px;width:100%;display:table}.hero_home.version_1 .content{background-color:#2f353abf}.hero_home .content{display:table-cell;padding:0;text-align:center;font-size:21px;font-size:1.3125rem;color:#fff;text-shadow:1px 1px 2px rgba(0,0,0,.25);background-color:#3e3f77;background-color:rgba(63,64,121,.9)}input.search-query{width:100%;height:50px;padding-left:20px;border:0;border-radius:3px;box-shadow:0 0 50px 0 rgba(0,0,0,.35);font-weight:500;font-size:16px;font-size:1rem;color:#333;margin:0 auto}.input-group{position:relative;display:flex;flex-wrap:wrap;align-items:stretch;width:80%;margin:0 auto}button.btn_search{position:absolute;transition:all .3s ease-in-out;right:0;color:#fff;font-weight:600;font-size:16px;font-size:1rem;top:0;border:0;padding:0 25px;height:50px;cursor:pointer;outline:0;border-radius:0 3px 3px 0;background-color:#74d1c6}h3.fadeInUp.animated{margin:0;font-size:60px;font-size:3.75rem;font-weight:700;color:#fff;padding:top 25px 30px}.customers{padding-top:60px}.customers h1{font-size:25px;padding-bottom:20px;font-weight:700;color:#3a4248;text-align:center}@media only screen and (max-width:767px){button.btn_search{position:inherit!important}h3.fadeInUp.animated{font-size:34px!important;padding-bottom:25px!important}}input.userinfodata{width:100%;border:1px solid #ccc;border-radius:11px;padding:10px;outline:0!important;background-color:#c8ced3a8}.col-md-8.user.data .col-md-6{margin-bottom:10px}img.userpic{width:120px}.userbuttons{padding-top:50px;padding-bottom:30px}.userbuttons .col-md-3{margin-bottom:15px}.col-md-4.user.image{text-align:center;padding-bottom:20px}"

/***/ }),

/***/ "./src/app/views/add catalog/add-catalog.component.html":
/*!**************************************************************!*\
  !*** ./src/app/views/add catalog/add-catalog.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"hero_home version_1\">\r\n    <div class=\"content\">\r\n      <h3 class=\"fadeInUp animated\">Find a Customer!</h3>\r\n      \r\n      <form [formGroup]=\"searchForm\" (ngSubmit)=\"onSubmit()\">\r\n        <div id=\"custom-search-input\">\r\n          <div class=\"input-group\">\r\n           \r\n            <input type=\"text\" class=\"search-query\" placeholder=\"Search by Mobile Number.....\" formControlName=\"mobile\" [ngClass]=\"{ 'is-invalid': submitted && f.mobile.errors }\" >\r\n            <button  class=\"btn_search\" >Search</button>\r\n            <div *ngIf=\"submitted && f.mobile.errors\" class=\"invalid-feedback\">\r\n                <div *ngIf=\"f.mobile.errors.required\">Mobile Number is required</div>\r\n                </div>\r\n          \r\n          </div>\r\n         \r\n        </div>\r\n      </form>\r\n    </div>\r\n  </div>\r\n\r\n  <div class=\"datausertab\" >\r\n      <table class=\"table responsive table-striped asasigned\" *ngIf=\"catuser >'0'\" >\r\n          <thead>\r\n              <tr>\r\n                  <th> Name</th>\r\n                  <th>Mobile</th>\r\n                  <th>Address line 1</th>\r\n                  <th>Address line 2</th>\r\n                 \r\n                </tr>\r\n          </thead>\r\n          <tbody>\r\n              <tr>\r\n                  <td>{{fullname}}</td>\r\n                  <td>{{mobile}}</td>\r\n                  <td>{{address1}}</td>\r\n                  <td>{{address2}}</td>\r\n              </tr>\r\n          </tbody>\r\n          <button [disabled]=\"loading\" class=\"btn btn-primary\" [routerLink]=\"['/add-catalog/transfer-catalog']\">Next</button>\r\n      </table>\r\n      \r\n      </div>\r\n  \r\n  \r\n  \r\n  "

/***/ }),

/***/ "./src/app/views/add catalog/add-catalog.component.ts":
/*!************************************************************!*\
  !*** ./src/app/views/add catalog/add-catalog.component.ts ***!
  \************************************************************/
/*! exports provided: addcatalog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addcatalog", function() { return addcatalog; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../_services */ "./src/app/_services/index.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var addcatalog = /** @class */ (function () {
    function addcatalog(formBuilder, userService, toastr) {
        this.formBuilder = formBuilder;
        this.userService = userService;
        this.toastr = toastr;
        this.loading = false;
        this.submitted = false;
        this.currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
    }
    addcatalog.prototype.ngOnInit = function () {
        this.searchForm = this.formBuilder.group({
            mobile: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
        });
    };
    Object.defineProperty(addcatalog.prototype, "f", {
        get: function () { return this.searchForm.controls; },
        enumerable: true,
        configurable: true
    });
    addcatalog.prototype.onSubmit = function () {
        var _this = this;
        this.submitted = true;
        // stop here if form is invalid
        //  console.log(this.f.mobile.value);
        this.userService.getusercatalog(this.f.mobile.value).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["first"])()).subscribe(function (result) {
            _this.catuser = result['users'];
            // console.log(result['users'])
            // console.log(this.catuser.fullname)
            _this.fullname = result['users'].fullname;
            _this.mobile = result['users'].mobile;
            _this.address1 = result['users'].address_line_1;
            _this.address2 = result['users'].address_line_2;
            //console.log(this.fullname)
        });
    };
    addcatalog = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            template: __webpack_require__(/*! ./add-catalog.component.html */ "./src/app/views/add catalog/add-catalog.component.html"),
            styles: [__webpack_require__(/*! ./add-catalog.component.css */ "./src/app/views/add catalog/add-catalog.component.css")],
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"], _services__WEBPACK_IMPORTED_MODULE_2__["UserService"], ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"]])
    ], addcatalog);
    return addcatalog;
}());



/***/ }),

/***/ "./src/app/views/add catalog/catalog-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/views/add catalog/catalog-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: CatalogRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CatalogRoutingModule", function() { return CatalogRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _add_catalog_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./add-catalog.component */ "./src/app/views/add catalog/add-catalog.component.ts");
/* harmony import */ var _transfer_catalog_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./transfer-catalog.component */ "./src/app/views/add catalog/transfer-catalog.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var routes = [
    {
        path: '',
        data: {
            title: 'Add catalog'
        },
        children: [
            {
                path: 'admin-catalog',
                component: _add_catalog_component__WEBPACK_IMPORTED_MODULE_2__["addcatalog"],
                data: {
                    title: 'Admin Catalog'
                }
            },
            {
                path: 'transfer-catalog',
                component: _transfer_catalog_component__WEBPACK_IMPORTED_MODULE_3__["transfercatalog"],
                data: {
                    title: 'Transfer Catalog'
                }
            },
        ]
    }
];
var CatalogRoutingModule = /** @class */ (function () {
    function CatalogRoutingModule() {
    }
    CatalogRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], CatalogRoutingModule);
    return CatalogRoutingModule;
}());



/***/ }),

/***/ "./src/app/views/add catalog/catalog.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/views/add catalog/catalog.module.ts ***!
  \*****************************************************/
/*! exports provided: CatalogModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CatalogModule", function() { return CatalogModule; });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-bootstrap/dropdown */ "./node_modules/ngx-bootstrap/dropdown/index.js");
/* harmony import */ var _catalog_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./catalog-routing.module */ "./src/app/views/add catalog/catalog-routing.module.ts");
/* harmony import */ var _add_catalog_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./add-catalog.component */ "./src/app/views/add catalog/add-catalog.component.ts");
/* harmony import */ var _transfer_catalog_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./transfer-catalog.component */ "./src/app/views/add catalog/transfer-catalog.component.ts");
/* harmony import */ var ngx_smart_modal__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-smart-modal */ "./node_modules/ngx-smart-modal/esm5/ngx-smart-modal.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// Dropdowns Component





// Buttons Routing
// Angular
var CatalogModule = /** @class */ (function () {
    function CatalogModule() {
    }
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Input"])(),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"])
    ], CatalogModule.prototype, "group", void 0);
    CatalogModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_0__["CommonModule"],
                _catalog_routing_module__WEBPACK_IMPORTED_MODULE_4__["CatalogRoutingModule"],
                ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_3__["BsDropdownModule"].forRoot(),
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ReactiveFormsModule"],
                ngx_smart_modal__WEBPACK_IMPORTED_MODULE_7__["NgxSmartModalModule"].forChild()
            ],
            declarations: [
                _add_catalog_component__WEBPACK_IMPORTED_MODULE_5__["addcatalog"],
                _transfer_catalog_component__WEBPACK_IMPORTED_MODULE_6__["transfercatalog"],
            ]
        })
    ], CatalogModule);
    return CatalogModule;
}());



/***/ }),

/***/ "./src/app/views/add catalog/transfer-catalog.component.css":
/*!******************************************************************!*\
  !*** ./src/app/views/add catalog/transfer-catalog.component.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".hero_home.version_1 {\r\n    background: url(/assets/customer.jpg) center bottom no-repeat #3f4079;\r\n\r\n}\r\n.hero_home {\r\n    height: 180px;\r\n    width: 100%;\r\n    display: table;\r\n}\r\n.hero_home.version_1 .content {\r\n    background-color: #2f353abf;\r\n\r\n}\r\n.hero_home .content {\r\n    display: table-cell;\r\n    padding: 0;\r\n\r\n    text-align: center;\r\n    font-size: 21px;\r\n    font-size: 1.3125rem;\r\n    color: #fff;\r\n    text-shadow: 1px 1px 2px rgba(0,0,0,.25);\r\n    background-color: #3e3f77;\r\n    background-color: rgba(63,64,121,.9);\r\n}\r\ninput.search-query {\r\n    width: 100%;\r\n    /* margin: 0; */\r\n    height: 50px;\r\n    padding-left: 20px;\r\n    border: 0;\r\n    border-radius: 3px;\r\n    box-shadow: 0 0 50px 0 rgba(0,0,0,.35);\r\n    font-weight: 500;\r\n    font-size: 16px;\r\n    font-size: 1rem;\r\n    color: #333;\r\n    margin: 0 auto;\r\n}\r\n.input-group {\r\n    position: relative;\r\n    display: flex;\r\n    flex-wrap: wrap;\r\n    align-items: stretch;\r\n    width: 80%;\r\n    margin: 0 auto;\r\n}\r\nbutton.btn_search {\r\n    position: absolute;\r\n    transition: all .3s ease-in-out;\r\n    right: 0;\r\n    color: #fff;\r\n    font-weight: 600;\r\n    font-size: 16px;\r\n    font-size: 1rem;\r\n    top: 0;\r\n    border: 0;\r\n    padding: 0 25px;\r\n    height: 50px;\r\n    cursor: pointer;\r\n    outline: 0;\r\n    border-radius: 0 3px 3px 0;\r\n    background-color: #74d1c6;\r\n}\r\nh3.fadeInUp.animated {\r\n    margin: 0;\r\n    font-size: 60px;\r\n    font-size: 2.75rem;\r\n    font-weight: 700;\r\n    color: #fff;\r\n    padding: top 25px;\r\n    padding-bottom: 30px;\r\n}\r\n.customers {\r\n    padding-top: 60px;\r\n}\r\n.customers h1 {\r\n    font-size: 25px;\r\n    padding-bottom: 20px;\r\n    font-weight: 700;\r\n    color: #3a4248;\r\n    text-align:center;\r\n}\r\n@media only screen and (max-width: 767px) {\r\n    button.btn_search {\r\n                position: inherit !important;\r\n            }\r\n            h3.fadeInUp.animated {\r\n               \r\n                font-size: 34px !important;\r\n                padding-bottom: 25px !important;\r\n            }\r\n}\r\ninput.userinfodata {\r\n    width: 100%;\r\n    border: 1px solid #ccc;\r\n    border-radius: 11px;\r\n    padding: 10px;\r\n    outline: none !important;\r\n    background-color: #c8ced3a8;\r\n}\r\n.col-md-8.user.data .col-md-6 {\r\n    margin-bottom: 10px;\r\n}\r\nimg.userpic {\r\n    width: 120px;\r\n}\r\n.userbuttons {\r\n    padding-top: 50px;\r\n    padding-bottom: 30px;\r\n}\r\n.userbuttons .col-md-3 {\r\n    margin-bottom: 15px;\r\n}\r\n.col-md-4.user.image {\r\n    text-align: center;\r\n    padding-bottom: 20px;\r\n}\r\n.demo-container {\r\n    position: relative;\r\n    width: 100%;\r\n    max-width: 960px;\r\n    height: 100vh;\r\n    margin: 0 auto;\r\n    padding: 20px;\r\n    box-sizing: border-box;\r\n    display: flex;\r\n    flex-direction: column;\r\n    flex-wrap: wrap;\r\n    justify-content: center;\r\n    align-items: center;\r\n}\r\n.overlay {\r\n    position: fixed;\r\n    top: 0;\r\n    bottom: 0;\r\n    left: 0;\r\n    right: 0;\r\n    overflow-x: hidden;\r\n    overflow-y: auto;\r\n    transition: background-color .5s;\r\n    background-color: transparent;\r\n    z-index: 999;\r\n}\r\n.nsm-dialog {\r\n    position: relative;\r\n    opacity: 1;\r\n    visibility: visible;\r\n    min-height: 200px;\r\n    width: 100%;\r\n    max-width: 520px;\r\n    margin: 0 auto;\r\n    pointer-events: none;\r\n}\r\n.nsm-content {\r\n    position: relative;\r\n    display: flex;\r\n    /* flex-direction: column; */\r\n    pointer-events: auto;\r\n    background-clip: padding-box;\r\n    background-color: #fff;\r\n    /* border-radius: 2px; */\r\n    padding: 1rem;\r\n    /* margin: 1.75rem; */\r\n    box-shadow: 0 7px 8px -4px rgba(0,0,0,.2), 0 13px 19px 2px rgba(0,0,0,.14), 0 5px 24px 4px rgba(0,0,0,.12);\r\n    outline: 0;\r\n    -webkit-transform: translate3d(0,0,0);\r\n    transform: translate3d(0,0,0);\r\n}"

/***/ }),

/***/ "./src/app/views/add catalog/transfer-catalog.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/views/add catalog/transfer-catalog.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"hero_home version_1\">\r\n        <div class=\"content\">\r\n          <h3 class=\"fadeInUp animated\">Find Seller/user to transfer the catalog</h3>\r\n          \r\n          <form [formGroup]=\"searchForm1\" (ngSubmit)=\"onSubmit()\">\r\n            <div id=\"custom-search-input\">\r\n              <div class=\"input-group\">\r\n               \r\n                <input type=\"text\" class=\"search-query\" placeholder=\"Search by Mobile Number.....\" formControlName=\"mobile\" [ngClass]=\"{ 'is-invalid': submitted && f.mobile.errors }\" >\r\n                <button  class=\"btn_search\" >Search</button>\r\n                <div *ngIf=\"submitted && f.mobile.errors\" class=\"invalid-feedback\">\r\n                    <div *ngIf=\"f.mobile.errors.required\">Mobile Number is required</div>\r\n                    </div>\r\n              \r\n              </div>\r\n             \r\n            </div>\r\n          </form>\r\n        </div>\r\n      </div>\r\n    \r\n      <div class=\"datausertab\">\r\n          <table class=\"table responsive table-striped asasigned\">\r\n              <thead>\r\n                  <tr>\r\n                      <th> Name</th>\r\n                      <th>Mobile</th>\r\n                      <th>Address</th>\r\n                     \r\n                    </tr>\r\n              </thead>\r\n              <tbody>\r\n                  <tr>\r\n                      <td>Admin</td>\r\n                      <td>123456789</td>\r\n                      <td>123</td>\r\n                  </tr>\r\n              </tbody>\r\n          </table>\r\n          \r\n          <button class=\"btn btn-primary\" (click)=\"myBootstrapModal.open()\">Transfer Product</button>\r\n          </div>\r\n          \r\n\r\n          <!-- <div class=\"demo-container\">\r\n            \r\n              \r\n            </div> -->\r\n            \r\n            <ngx-smart-modal #myBootstrapModal identifier=\"myBootstrapModal\" [closable]=\"false\"\r\n                             customClass=\"modal-content no-padding\">\r\n              <div class=\"modal-header\">\r\n                <h5 class=\"modal-title\">Modal title</h5>\r\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\" (click)=\"myBootstrapModal.close()\">\r\n                  <span aria-hidden=\"true\">&times;</span>\r\n                </button>\r\n              </div>\r\n              <div class=\"modal-body\">\r\n                <h6>NgxSmartModal doesn't interfere with any CSS framework</h6>\r\n                <img src=\"https://lorempicsum.com/futurama/1000/400/6\" class=\"img-fluid rounded\" alt=\"Responsive image\">\r\n                <p class=\"py-3\">Modal body text goes here.</p>\r\n              </div>\r\n              <div class=\"modal-footer\">\r\n                <button type=\"button\" class=\"btn btn-primary\">Save changes</button>\r\n                <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\" (click)=\"myBootstrapModal.close()\">Close\r\n                </button>\r\n              </div>\r\n            </ngx-smart-modal>"

/***/ }),

/***/ "./src/app/views/add catalog/transfer-catalog.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/views/add catalog/transfer-catalog.component.ts ***!
  \*****************************************************************/
/*! exports provided: transfercatalog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "transfercatalog", function() { return transfercatalog; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ngx_smart_modal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-smart-modal */ "./node_modules/ngx-smart-modal/esm5/ngx-smart-modal.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var transfercatalog = /** @class */ (function () {
    function transfercatalog(formBuilder, ngxSmartModalService) {
        this.formBuilder = formBuilder;
        this.ngxSmartModalService = ngxSmartModalService;
        this.loading = false;
        this.submitted = false;
    }
    transfercatalog.prototype.ngOnInit = function () {
        this.searchForm1 = this.formBuilder.group({
            mobile: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
        });
    };
    Object.defineProperty(transfercatalog.prototype, "f", {
        get: function () { return this.searchForm1.controls; },
        enumerable: true,
        configurable: true
    });
    transfercatalog.prototype.onSubmit = function () {
        this.submitted = true;
        // stop here if form is invalid
        console.log(this.f.mobile.value);
    };
    transfercatalog = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            template: __webpack_require__(/*! ./transfer-catalog.component.html */ "./src/app/views/add catalog/transfer-catalog.component.html"),
            styles: [__webpack_require__(/*! ./transfer-catalog.component.css */ "./src/app/views/add catalog/transfer-catalog.component.css")],
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"], ngx_smart_modal__WEBPACK_IMPORTED_MODULE_2__["NgxSmartModalService"]])
    ], transfercatalog);
    return transfercatalog;
}());



/***/ })

}]);
//# sourceMappingURL=views-add-catalog-catalog-module.js.map